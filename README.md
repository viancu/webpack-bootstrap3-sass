# README #

How to get the application up and running.

### What is this repository for? ###

* This is a [Webpack](https://webpack.github.io/) starter kit for developing apps with Bootstrap 3 and SASS.
* This project has two branches: master and customers-app. Master holds the skeleton app and customers-app is the example.

### How do I get set up? ###

* Install nodejs from  https://nodejs.org/en/download/current, the node package manager is included.
* Install git or download as zip, ignore the line below if you downloaded it.
* git clone https://bitbucket.org/viancu/webpack-bootstrap3-sass my-project
* cd my-project
* npm install
* Run the dev mode: npm run dev
* Open browser and go to localhost:8080
* Modify something somewhere inside src and it would be automatic recompiled and the browser refreshed.
* To build a deployable app run: npm run build
* dist folder is created and inside you would find index.html and build.js to distribute